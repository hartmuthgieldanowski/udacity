'''
From the instructions:
    Predict flower name from an image with predict.py along with the probability of that name. That is, you'll pass in a single image /path/to/image and       return the flower name and class probability.
    Basic usage: python predict.py /path/to/image checkpoint
    Options:
        Return top 
        K most likely classes: python predict.py input checkpoint --top_k 3
        Use a mapping of categories to real names: python predict.py input checkpoint --category_names cat_to_name.json
        Use GPU for inference: python predict.py input checkpoint --gpu

'''

# Imports here
import torch, torchvision, json, argparse
import numpy as np
from torchvision import datasets, transforms, models
from torch.autograd import Variable
from PIL import Image

#default parameters
data_dir_param = ''
model_dir_param = None
test_image_param = 'flowers/test/101/image_07949.jpg'
topk_param = 5
gpu_param = True
model_path = 'model_checkpoint.pth'
mapping_labels_param = 'cat_to_name.json'


#define parser inputs
def parser_arguments():
    parser = argparse.ArgumentParser(description = "Run classification for Images")
    parser.add_argument('--image_path', type = str, default = test_image_param, help = 'Select Image')
    parser.add_argument('--model_path', type = str, default = model_path, help = 'Select model path')
    parser.add_argument('--topk', type = int, default = topk_param, help = 'Define amount of the most likely classifications')
    parser.add_argument('--mapping_labels', type = str, default = mapping_labels_param, help = 'Select file for mapping labels')
    parser.add_argument('--gpu', action = "store_true", default = gpu_param, help = 'Select if GPU should be used')

    args = parser.parse_args()
    
    return args.image_path, args.model_path, args.topk, args.mapping_labels, args.gpu

#load the trained model from model_checkpoint
def load_model(file_path):
    model_checkpoint = torch.load(file_path)
    model = getattr(torchvision.models, model_checkpoint['network'])(pretrained = True)
    learning_rate = model_checkpoint['learning_rate']    
    model.classifier = model_checkpoint['classifier']
    model.epochs = model_checkpoint['epochs']
    model.optimizer = model_checkpoint['optimizer']
    model.load_state_dict(model_checkpoint['state_dict'])
    model.class_to_idx = model_checkpoint['class_to_idx']
     
    return model

#load, pre-process the image that needs to be classified
def prepare_image(pil_image):
    load_img = transforms.Compose([transforms.Resize(256),
                                   transforms.CenterCrop(224),
                                   transforms.ToTensor()])
    
    pil_image = load_img(pil_image).float()
    np_image = np.array(pil_image)
    
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    np_image = (np.transpose(np_image, (1, 2, 0)) - mean) / std
    np_image = np.transpose(np_image, (2, 0, 1))
    
    return np_image

#get prediction for image
def run_prediction(image_path, model, topk_ps = topk_param):
    #check if GPU is available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    #load model to GPU/CPU
    model.to(device)
    
    #open image
    pil_image = Image.open(image_path)
    #prepare image
    np_image = prepare_image(pil_image)
    #create tensor for processing
    tensor_image = torch.from_numpy(np_image)
    input = Variable(tensor_image)
    if torch.cuda.is_available():
        inputs = Variable(tensor_image.float().cuda())
        
    inputs = inputs.unsqueeze(dim=0)
    log_ps = model.forward(inputs)
    ps = torch.exp(log_ps)
    
    top_ps, top_classifications = ps.topk(topk_ps, dim = 1)
    
    class_to_idx_inverted = {model.class_to_idx[c]: c for c in model.class_to_idx}
    top_mapped_labels = list()
    
    for label in top_classifications.cpu().detach().numpy()[0]:
        top_mapped_labels.append(class_to_idx_inverted[label])
        
    return top_ps.cpu().detach().numpy()[0], top_mapped_labels

#run label mappping for categories
def run_labelmapping(mapping_labels):    
    with open(mapping_labels, 'r') as f:
        mapping_labels_img = json.load(f)

    return mapping_labels_img

#load the model and map categories to labels of Flower Types
def get_model(mapping_labels, model_path):
    model = load_model(model_path)
    mapping_labels_img = run_labelmapping(mapping_labels)
    
    return model, mapping_labels_img

#get probabilities and labels for prediction 
def probabilities(image_path, model_path, topk, mapping_labels, gpu):
    model, mapping_labels_img = get_model(mapping_labels, model_path)
    
    top_ps, top_classifications = run_prediction(image_path, model, topk_ps = topk)
    
    print('Certainity: ', top_ps)
    print('Flower Types: ', [mapping_labels_img[c] for c in top_classifications])
    
    #if loop for test images - print real path
    if image_path == test_image_param:
     path_parts = image_path.split('/')
     true_label = path_parts[-2]
     print('True Flower Type: ', mapping_labels_img[true_label])
        
#main function for running prediction on an image
if __name__ == "__main__":
    image_path, model_path, topk, mapping_labels, gpu = parser_arguments()
    
    probabilities(image_path, model_path, topk, mapping_labels, gpu)
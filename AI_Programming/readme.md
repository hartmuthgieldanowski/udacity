# Exercises
Scrap books and learning exercises from the course

# ImageClassifier
Final project on the course building a simple image classifier with a pre-trained model. Complete training, validation and testing sequence as command-line project.

# License
Please adhere to the creative commons [(CC-by-SA)](https://creativecommons.org/licenses/by-sa/4.0)